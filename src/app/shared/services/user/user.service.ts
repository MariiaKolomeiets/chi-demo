import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private _investments$ = new BehaviorSubject<number>(0);
  private _loans$ = new BehaviorSubject<string[]>([]);

  constructor() {
  }

  addInvest(sum: number, id: string): void {
    const prevSumm = this.getInvestmentsVal();
    const prevLoans = this._loans$.value;
    const total = Number(prevSumm) + Number(sum);
    this._investments$.next(total);
    this._loans$.next([].concat(prevLoans, id));
  }

  getInvestmentsVal(): number {
    return this._investments$.value;
  }

  getInvestments(): Observable<number> {
    return this._investments$.asObservable();
  }

  checkInvestId(id): boolean {
    const loans = this._loans$.value;
    return loans.includes(id);
  }
}
