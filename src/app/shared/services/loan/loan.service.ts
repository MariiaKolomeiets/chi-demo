import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, of, Subject} from 'rxjs';
import {dataLoans} from '../../data/mock.data';
import {ILoan} from '../../models/loan/loan.model';

@Injectable({
  providedIn: 'root'
})
export class LoanService {
  private _lonsData$ = new BehaviorSubject<ILoan[]>(null);

  constructor() {
  }

  getRequestLoans(): void {
    this._lonsData$.next(dataLoans.loans);
  }

  getListLoans(): Observable<ILoan[]> {
    return this._lonsData$.asObservable();
  }

  getLoan(id: string): ILoan {
    const findLoan = this._lonsData$.value.find((e: ILoan) => e.id == id);
    return findLoan;
  }

  investInLoan(id: string, summ: number): Observable<boolean | null> {
    const loans = this._lonsData$.value;
    const findLoan: ILoan = loans.find((e: ILoan) => e.id == id);
    if (findLoan) {
      findLoan.amount = (Number(findLoan.amount) - Number(summ)).toFixed(3);
      findLoan.available = (Number(findLoan.available) - Number(summ)).toFixed(3);
    }
    return findLoan ? of(true) : of(null);
  }

}
