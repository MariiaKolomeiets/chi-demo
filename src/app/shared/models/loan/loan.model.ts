export interface ILoan {
  id: string;
  title: string;
  tranche: string;
  available: string | number;
  annualised_return: string | number;
  term_remaining: string | number;
  ltv: string | number;
  amount: string | number;
}
