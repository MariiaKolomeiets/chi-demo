import {Component, OnDestroy, OnInit} from '@angular/core';
import {UserService} from '../services/user/user.service';
import {takeUntil} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Component({
  selector: 'app-loans-amount',
  templateUrl: './loans-amount.component.html',
  styleUrls: ['./loans-amount.component.scss']
})
export class LoansAmountComponent implements OnInit, OnDestroy {
  public amountInvestments = 0;
  private _destroy$: Subject<boolean> = new Subject<boolean>();
  constructor(protected userService: UserService) {
  }

  ngOnInit() {
    this.userService.getInvestments().pipe(takeUntil(this._destroy$)).subscribe(
      (amount: number) => {
        this.amountInvestments = amount;
      }
    );
  }

  ngOnDestroy() {
    this._destroy$.next(true);
    this._destroy$.unsubscribe();
  }
}
