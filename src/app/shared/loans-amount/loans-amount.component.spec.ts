import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoansAmountComponent } from './loans-amount.component';

describe('LoansAmountComponent', () => {
  let component: LoansAmountComponent;
  let fixture: ComponentFixture<LoansAmountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoansAmountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoansAmountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
