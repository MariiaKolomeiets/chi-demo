export class ModalConfig<D = any> {
  type: string;
  data?: D;
  blur?: boolean;
  rememberState?: boolean;
  blurType?: string;
  styles?: IModalStyles;
}

export interface IModalStyles {
  'background-color': string;
}
