import {
  Component,
  Type,
  OnDestroy,
  AfterViewInit,
  ViewChild,
  ComponentFactoryResolver,
  ComponentRef,
  ChangeDetectorRef,
  OnInit,
  Renderer2
} from '@angular/core';
import {InsertionDirective} from '../insertion.directive';
import {ModalRef} from '../modal-ref';
import {ModalConfig} from '../modalConfig';
import {modalAnimations} from './modalAnimationConfig';
import {Router} from '@angular/router';


@Component({
  selector: 'app-modal-container',
  templateUrl: './modal-container.component.html',
  styleUrls: ['./modal-container.component.scss']
})
export class ModalContainerComponent implements OnInit, AfterViewInit, OnDestroy {

  componentRef: ComponentRef<any>;
  childComponentType: Type<any>;
  animateModalClass: string;
  typeModal: string;

  @ViewChild(InsertionDirective, {static: false}) insertionPoint: InsertionDirective;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private cd: ChangeDetectorRef,
    private modal: ModalRef,
    public config: ModalConfig,
    private router: Router,
  ) {
    this.typeModal = this.config.type;
  }

  ngOnInit() {

  }

  ngAfterViewInit() {
    this.loadChildComponent(this.childComponentType);
    this.cd.detectChanges();
  }

  ngOnDestroy() {
    if (this.componentRef) {
      this.componentRef.destroy();
    }
  }

  public onOverlayClicked(evt?: MouseEvent) {
    this.modal.close();
  }

  onDialogClicked(evt: MouseEvent) {
    evt.stopPropagation();
  }

  loadChildComponent(componentType: Type<any>) {
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(componentType);

    const viewContainerRef = this.insertionPoint.viewContainerRef;
    viewContainerRef.clear();

    this.componentRef = viewContainerRef.createComponent(componentFactory);
  }

}
