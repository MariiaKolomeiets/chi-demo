export const modalAnimations = {
  sign: {
    open: 'zoom-out',
    close: 'zoom-out-close',
    durationIn: 300,
    durationOut: 300
  },
  loaderBet: {
    open: 'show',
    close: 'zoom-out-close',
    durationIn: 0,
    durationOut: 300
  },
  bet: {
    open: 'show-bet',
    close: 'zoom-out-close',
    durationIn: 0,
    durationOut: 300
  },
    betslip: {
        open: 'zoom-out',
        close: 'zoom-out-close',
        durationIn: 300,
        durationOut: 500
    },
  setting: {
    open: 'grow-up',
    close: 'grow-down-close',
    durationIn: 300,
    durationOut: 500
  },
  copied: {
    open: 'zoom-out',
    close: 'zoom-out-close',
    durationIn: 0,
    durationOut: 0
  },
  default: {
    open: 'show',
    close: '',
    durationIn: 0,
    durationOut: 300
  }
};
