import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoansMainComponent} from './+loans/loans-main/loans-main.component';
import {LoansListComponent} from './+loans/loans-list/loans-list.component';
import {LoansItemComponent} from './+loans/loans-item/loans-item.component';
import {LoansAmountComponent} from './shared/loans-amount/loans-amount.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {DynamicModalService} from './shared/dynamic-modal/dynamic-modal-service/modal-service.service';
import {ModalRef} from './shared/dynamic-modal/modal-ref';
import {InvestModalComponent} from './+loans/shared/invest-modal/invest-modal.component';
import {ModalContainerComponent} from './shared/dynamic-modal/modal-container/modal-container.component';
import {InsertionDirective} from './shared/dynamic-modal/insertion.directive';
import {MatInputModule} from '@angular/material/input';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
@NgModule({
  entryComponents: [InvestModalComponent, ModalContainerComponent],
  declarations: [
    AppComponent,
    LoansMainComponent,
    LoansListComponent,
    LoansItemComponent,
    LoansAmountComponent,
    InvestModalComponent,
    ModalContainerComponent,
    InsertionDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [DynamicModalService, ModalRef],
  bootstrap: [AppComponent]
})
export class AppModule {
}
