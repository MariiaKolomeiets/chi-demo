import {Component, OnDestroy, OnInit} from '@angular/core';
import {dataLoans} from '../../shared/data/mock.data';
import {LoanService} from '../../shared/services/loan/loan.service';
import {ILoan} from '../../shared/models/loan/loan.model';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-loans-main',
  templateUrl: './loans-main.component.html',
  styleUrls: ['./loans-main.component.scss']
})
export class LoansMainComponent implements OnInit, OnDestroy {
  public loans: ILoan[];
  private _destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(protected loanService: LoanService) {
  }

  ngOnInit() {
    this.loanService.getRequestLoans();

    this.loanService.getListLoans().pipe(takeUntil(this._destroy$)).subscribe((loanData: ILoan[]) => {
        this.loans = loanData;
      }
    );

  }

  ngOnDestroy() {
    this._destroy$.next(true);
    this._destroy$.unsubscribe();
  }
}
