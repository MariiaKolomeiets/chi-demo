import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoansMainComponent } from './loans-main.component';

describe('LoansMainComponent', () => {
  let component: LoansMainComponent;
  let fixture: ComponentFixture<LoansMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoansMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoansMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
