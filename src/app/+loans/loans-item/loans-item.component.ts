import {Component, Input, OnInit} from '@angular/core';
import {ILoan} from '../../shared/models/loan/loan.model';
import {DynamicModalService} from '../../shared/dynamic-modal/dynamic-modal-service/modal-service.service';
import {InvestModalComponent} from '../shared/invest-modal/invest-modal.component';
import {LoanService} from '../../shared/services/loan/loan.service';
import {UserService} from '../../shared/services/user/user.service';

@Component({
  selector: 'app-loans-item',
  templateUrl: './loans-item.component.html',
  styleUrls: ['./loans-item.component.scss']
})
export class LoansItemComponent implements OnInit {
  @Input() loan: ILoan;

  constructor(private modalService: DynamicModalService,
              private loanService: LoanService,
              private userService: UserService) {
  }

  ngOnInit() {
  }

  openInvestModal(id) {
    const loan = this.loanService.getLoan(id);
    if (loan) {
      this.modalService.open(InvestModalComponent, {type: 'invest', data: loan, rememberState: true});
    }
  }

  checkInvestId(id: string) {
    return this.userService.checkInvestId(id);
  }
}
