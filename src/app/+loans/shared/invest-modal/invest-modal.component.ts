import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModalConfig} from '../../../shared/dynamic-modal/modalConfig';
import {ILoan} from '../../../shared/models/loan/loan.model';
import {LoanService} from '../../../shared/services/loan/loan.service';
import {ModalRef} from '../../../shared/dynamic-modal/modal-ref';
import {UserService} from '../../../shared/services/user/user.service';

@Component({
  selector: 'app-invest-modal',
  templateUrl: './invest-modal.component.html',
  styleUrls: ['./invest-modal.component.scss']
})
export class InvestModalComponent implements OnInit {
  public investForm: FormGroup;
  public infoLoan: ILoan;

  constructor(private formBuilder: FormBuilder,
              private config: ModalConfig,
              private loanService: LoanService,
              private modalRef: ModalRef,
              private userService: UserService) {
  }

  ngOnInit() {
    this._initInvestForm();
    this.infoLoan = this.config.data;
  }

  get f() {
    return this.investForm.controls;
  }

  private _initInvestForm() {
    this.investForm = this.formBuilder.group({
      amount: [null, [Validators.required, Validators.min(5), Validators.max(this.config.data.available)]],
    });
  }

  public invest() {
    const {id} = this.infoLoan;
    const {amount} = this.investForm.value;

    this.loanService.investInLoan(id, amount).subscribe(
      status => {
        if (status) {
          this.userService.addInvest(amount, id);
          this.modalRef.close();
        }
      }
    );
  }
}
