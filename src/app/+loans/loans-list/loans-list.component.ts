import {Component, Input, OnInit} from '@angular/core';
import {ILoan} from '../../shared/models/loan/loan.model';

@Component({
  selector: 'app-loans-list',
  templateUrl: './loans-list.component.html',
  styleUrls: ['./loans-list.component.scss']
})
export class LoansListComponent implements OnInit {
  @Input() loans: ILoan[];
  constructor() { }

  ngOnInit() {
  }

}
